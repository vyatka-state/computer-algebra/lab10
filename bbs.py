from time import time



class BSSGenerator:
    def __init__(self, x0, p, q):
        self.x0 = x0
        self.x_current = x0
        self.p = p
        self.q = q

    def in_position(self, n):
        return (self.x0**(2**n % ((self.p - 1) * (self.q - 1)))) % (self.p * self.q)

    def next(self):
        self.x_current = (self.x_current**2) % (self.p * self.q)
        return self.x_current


def main():
    p = 2047
    q = 8191
    x0 = 71
    bss_generator = BSSGenerator(x0, p, q)

    iteration_count = int(input('Введите количество итераций: '))

    print("Последовательной формулой")
    period_length = 0
    period_start_index = 1
    randoms = []
    start_time = time()
    for i in range(iteration_count):
        randoms.append(bss_generator.next())
        if i > 1 and period_length < period_start_index:
            if randoms[period_length + 1] == randoms[i]:
                if period_length == 0:
                    period_start_index = i
                period_length += 1
            else:
                period_length = 0
                period_start_index = 1
        print(randoms[i])
    end_time = time()
    print(f'Время выполнения: {end_time - start_time}')
    print(f'Длина периода: {period_length}')
    if period_length != 0 and period_length != period_start_index:
        print('Это длина незаконченного периода. При большем количестве итераций период будет больше')

    print("\nНепоследовательной формулой")
    for i in range(iteration_count):
        print(bss_generator.in_position(i + 1))


if __name__ == '__main__':
    main()
