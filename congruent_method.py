from time import time


class CongruentGenerator:
    def __init__(self, a, mod, x0):
        self.a = a
        self.mod = mod
        self.current = x0

    def next(self):
        self.current = (self.a * self.current) % self.mod
        return self.current


def main():
    mod = int(2 ** 31 - 1)
    a = int(7 ** 5)
    x0 = int(input('Введите начальное число: '))
    congruent_generator = CongruentGenerator(a, mod, x0)

    n = int(input('Введите количество итераций: '))

    period_length = 0
    period_start_index = 1
    randoms = []
    start_time = time()
    for i in range(n):
        randoms.append(congruent_generator.next())
        if i > 1 and period_length < period_start_index:
            if randoms[period_length] == randoms[i]:
                if period_length == 0:
                    period_start_index = i
                period_length += 1
            else:
                period_length = 0
                period_start_index = 1
        print(randoms[i])
    end_time = time()
    print(f'Время выполнения: {end_time - start_time} с.')
    print(f'Длина периода: {period_length}')
    if period_length != 0 and period_length != period_start_index:
        print('Это длина незаконченного периода. При большем количестве итераций период будет больше')


if __name__ == '__main__':
    main()
