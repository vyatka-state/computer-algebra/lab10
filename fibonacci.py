from congruent_method import CongruentGenerator
from time import time


class FibonacciGenerator:
    def __init__(self, a, b, x0, mod, coeff):
        self.a = a
        self.b = b
        congruent_generator = CongruentGenerator(coeff, mod, x0)
        self.X = []
        self.k = -1
        for i in range(max(a, b)):
            self.k += 1
            self.X.append(float(f"0.{congruent_generator.next()}"))

    def next(self):
        self.k = (self.k + 1) % max(self.a, self.b)
        a_index = (self.k - self.a) % max(self.a, self.b)
        b_index = (self.k - self.b) % max(self.a, self.b)

        if self.X[a_index] < self.X[b_index]:
            self.X[self.k] = self.X[a_index] - self.X[b_index] + 1
        else:
            self.X[self.k] = self.X[a_index] - self.X[b_index]

        return self.X[self.k]


def main():
    a = int(input('Введите a: '))
    b = int(input('Введите b: '))
    x0 = int(input('Введите начальное число: '))
    digits_after_dot = int(input('Введите количество цифр после запятой: '))

    # print(f'Период по формуле: {(pow(2, max(a, b)) - 1) * pow(2, digits_after_dot.bit_length())}')
    fibonacci_generator = FibonacciGenerator(a, b, x0, int(2**31 - 1), int(7**5))

    iteration_count = int(input('Введите количество итераций: '))

    period_length = 0
    period_start_index = 1
    randoms = []
    start_time = time()
    for i in range(iteration_count):
        randoms.append(round(fibonacci_generator.next(), digits_after_dot))
        if i > 1 and period_length < period_start_index:
            if randoms[period_length] == randoms[i]:
                if period_length == 0:
                    period_start_index = i
                period_length += 1
            else:
                period_length = 0
                period_start_index = 1
        print(randoms[i])
    end_time = time()
    print(f'Время выполнения: {end_time - start_time} с.')
    print(f'Длина периода: {period_length}')
    if period_length != 0 and period_length != period_start_index:
        print('Это длина незаконченного периода. При большем количестве итераций период будет больше')


if __name__ == '__main__':
    main()
